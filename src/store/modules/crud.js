import {loginByEmail, logout, getInfo} from 'api/login';

const user = {
    state: {
        page: '',
        size: '',
        setting: {
            articlePlatform: []
        }
    },
    mutations: {
        SET_PAGE: (state, page) => {
            state.page = page;
        },
        SET_SIZE: (state, size) => {
            state.size = size;
        }
    },
    actions: {
        // 获取用户信息
        getPage({commit, state}) {
            return new Promise((resolve, reject) => {
                getPage(page, size).then(response => {
                    const data = response.data;
                    commit('SET_PAGE', data.number);
                    commit('SET_SIZE', data.size);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },


    }
};

export default user;
